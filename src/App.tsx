import React from "react";
import EmployeeListScreen from "./screens/EmployeeListScreen";
const App = () => {
  return (
    <div>
      <EmployeeListScreen />
    </div>
  );
};

export default App;
