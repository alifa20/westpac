import React from "react";
import { HeaderWrapper, Title, Left, Right, Content } from "./styled";
import dayjs from "dayjs";
interface Props {
  companyInfo: {
    companyName: string;
    companyMotto: string;
    companyEst: string;
  };
}
const Header = ({ companyInfo }: Props) => {
  if (!companyInfo) {
    return null;
  }

  const { companyName = "", companyMotto = "", companyEst = "" } = companyInfo;
  return (
    <HeaderWrapper>
      {companyName.length > 0 && <Title>{companyName}</Title>}

      <Content>
        {companyMotto.length > 0 && <Left>{companyMotto}</Left>}
        {companyEst.length > 0 && (
          <Right>
            {`Established since ${dayjs(companyEst).format("DD/MM/YYYY")}`}
          </Right>
        )}
      </Content>
    </HeaderWrapper>
  );
};

export default Header;
