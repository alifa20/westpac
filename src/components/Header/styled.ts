import styled from "styled-components";

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  background-color: #eee;
  border-bottom: 1px #ccc solid;
`;

export const Title = styled.div`
  font-size: 30px;
  display: flex;
`;

export const Left = styled.div`
  align-content: flex-start;
`;

export const Right = styled.div`
  margin-left: auto;
`;

export const Content = styled.div`
  display: flex;
  padding-bottom: 20px;
`;
