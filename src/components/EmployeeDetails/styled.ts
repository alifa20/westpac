import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  z-index: 1;
  position: absolute;
  height: 100%;
  width: 100%;
  flex: 1;

  /* background: blue; */
  background-color: rgba(138, 138, 138, 0.4);
  align-items: center;
  justify-content: center;
`;

export const Inner = styled.div`
  height: 650px;
  width: 650px;
  border: 1px white solid;
  display: flex;
  flex-direction: column;
`;

export const Top = styled.div`
  background: transparent;
`;

export const Content = styled.div`
  background: white;
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export const Close = styled.div`
  margin-left: auto;
  padding: 5px 15px 5px 5px;
  font-size: 40px;
  width: 30px;
  cursor: pointer;
`;

export const Header = styled.div`
  display: flex;
  padding: 30px;
  flex-direction: row;
  height: 130px;
`;

export const H1 = styled.div`
  font-size: 30px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  border-bottom: 4px #ddd solid;
  margin-left: 40px;
`;

export const Body = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0 25px;
`;

export const BodyLeft = styled.div`
  display: flex;
  flex: 0.25;
`;

export const BodyRight = styled.div`
  flex: 0.7;
  margin-left: 30px;
`;
