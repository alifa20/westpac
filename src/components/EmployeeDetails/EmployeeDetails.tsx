import React, { useEffect, useRef } from "react";
import {
  Container,
  Inner,
  Header,
  H1,
  Close,
  Content,
  Body,
  BodyLeft,
  BodyRight
} from "./styled";
import { Employee } from "../Card/Card";
import dayjs from "dayjs";

interface Props {
  employee: Employee;
  onClose: Function;
}

const EmployeeDetails = ({ employee, onClose }: Props) => {
  const node: any = useRef();

  useEffect(() => {
    const handleClick = (e: any) => {
      if (node.current.contains(e.target)) {
        // 🐨 inside click
        return;
      }
      // 🐨 outside click
      onClose(employee.id);
    };

    // add when mounted
    document.addEventListener("mousedown", handleClick);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [employee.id, onClose]);

  return (
    <Container>
      <Inner ref={node}>
        <Close onClick={() => onClose(employee.id)}>X</Close>
        {/* <Top>
        </Top> */}
        <Content>
          <Header>
            <img
              src={employee.avatar}
              alt={`${employee.firstName} ${employee.lastName}`}
            />
            <H1>
              {employee.firstName} {"  "} {employee.lastName}
            </H1>
          </Header>
          <Body>
            <BodyLeft>
              {employee.jobTitle} <br />
              <br />
              Age: {employee.age} <br />
              Joined since <br />
              {`${dayjs(employee.dateJoined).format("DD/MM/YYYY")}`}
              <br />
            </BodyLeft>
            <BodyRight>{employee.bio}</BodyRight>
          </Body>
        </Content>
      </Inner>
    </Container>
  );
};

export default EmployeeDetails;
