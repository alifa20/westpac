import React from "react";
import { Container, Content, Header, Body } from "./styled";

export type Employee = {
  id: string;
  avatar: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  age: number;
  bio: string;
  dateJoined: string;
};

interface Props {
  employee: Employee & { isSelected: boolean };
  onClick: Function;
}

const Card = ({ employee, onClick }: Props) => {
  if (!employee) return null;

  const handleClick = () => onClick(employee.id);

  return (
    <Container onClick={handleClick} isSelected={employee.isSelected}>
      <img
        src={employee.avatar}
        alt={`${employee.firstName} ${employee.lastName}`}
      />
      <Content>
        <Header>
          {employee.firstName} {"  "} {employee.lastName}
        </Header>
        <Body>{String(employee.bio).substr(0, 50)}</Body>
      </Content>
    </Container>
  );
};

export default Card;
