import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 300px;
  flex-direction: row;
  margin: 10px;
  border: ${(props: { isSelected: boolean }) =>
    props.isSelected ? "1px red solid" : "1px #ddd solid"};
  cursor: pointer;
`;

export const Content = styled.div`
  padding: 10px;
`;

export const Header = styled.div`
  font-style: bold;
`;

export const Body = styled.div`
  margin-top: 10px;
`;
