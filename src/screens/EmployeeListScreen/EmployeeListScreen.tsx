import React, { useEffect, useState } from "react";
import Header from "../../components/Header";
import data from "./data/sample-data.json";
import { Content } from "./styled";
import Card from "../../components/Card";
import { Employee } from "../../components/Card/Card";
import EmployeeDetails from "../../components/EmployeeDetails";

const EmployeeListScreen = () => {
  const [companyInfo, setCompanyInfo] = useState();
  const [employees, setEmployees] = useState();
  const [selectedEmployee, setSelectedEmployee] = useState();

  useEffect(() => {
    setCompanyInfo(data.companyInfo);
    const employees = data.employees.map(employee => ({
      ...employee,
      isSelected: false
    }));
    setEmployees(employees);
  }, []);

  const handleCardSelected = (id: string) => {
    const found = employees.find((employee: Employee) => employee.id === id);
    const newState = employees.map((employee: Employee) =>
      employee.id === id
        ? { ...employee, isSelected: true }
        : { ...employee, isSelected: false }
    );
    setEmployees(newState);

    setSelectedEmployee(found);
  };

  const handleClose = (id: string) => {
    setSelectedEmployee(undefined);
    const newState = employees.map((employee: Employee) =>
      employee.id === id ? { ...employee, isSelected: false } : employee
    );
    setEmployees(newState);
  };

  return (
    <>
      <Header companyInfo={companyInfo} />

      <Content>
        {employees &&
          employees.map((employee: Employee & { isSelected: boolean }) => (
            <Card
              key={employee.id}
              employee={employee}
              onClick={handleCardSelected}
            ></Card>
          ))}

        {selectedEmployee && (
          <EmployeeDetails employee={selectedEmployee} onClose={handleClose} />
        )}
      </Content>
    </>
  );
};

export default EmployeeListScreen;
